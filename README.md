# LIQUID Boilerplate #

Estructura básica y limpia para empezar un proyecto de paginación. Alineado a las reglas de desarrollo de tecnología.

### Objetivo ###

* Archivos limpios para empezar un proyecto de maquetación.
* Carpetas ordenadas y nombradas de acuerdo a los lineamientos de desarrollo.
* Versión de jQuery estandar.
* Uso de HTML5
* Uso de normalize.

### Contenido ###

* **index.html** plantilla HTML limpia para empezar un proyecto. 
* **404.html** plantilla HTML para cuando hay un error.
* **main.js** contiene todas las funciones javascript.
* **plugin.js** contiene todas las funciones genéricas o plugins utilizados.
* **robots.txt** archivo robots para activar/bloquear indexación de buscadores
* **/css** carpeta donde se almacenan todas las hojas de estilos
* **/js** carpeta donde se almacenan todos los archivos javascript
* **/img** carpeta donde se almacenan todas las imágenes estáticas
* **/files** carpeta donde se almacenan las imágenes dinámicas, que cambiarán en la integración con backend.
* **/fonts** carpeta donde se almacenan todos los archivos de tipografías
* Deployment instructions

### Guías para Mejoras ###

* Agregar las sugerencias como Issue
* Documentar la razón de las recomendaciones.
* Responsable: Jim Peñaloza
